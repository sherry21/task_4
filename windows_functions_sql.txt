All database users know about regular aggregate functions which operate on an entire table and are used with a GROUP BY clause. 
But very few people use Window functions in SQL. These operate on a set of rows and return a single aggregated value for each row.
The main advantage of using Window functions over regular aggregate functions is: 
Window functions do not cause rows to become grouped into a single output row, the rows retain their separate identities and an aggregated value will be added to each row.
Window functions operate on a set of rows and return a single aggregated value for each row. The term Window describes the set of rows in the database on which the function will operate.
We define the Window (set of rows on which functions operates) using an OVER() clause.
Syntax:�
window_function ( [ ALL ] expression ) 
OVER ( [ PARTITION BY partition_list ] [ ORDER BY order_list] )
Types of window functions:
>Aggregate Window Functions 
	SUM(), MAX(), MIN(), AVG(). COUNT()
>Ranking Window Functions 
	RANK(), DENSE_RANK(), ROW_NUMBER(), NTILE()
>Value Window Functions 
	LAG(), LEAD(), FIRST_VALUE(), LAST_VALUE()

Aggregate Window Functions
SUM()
It does the sum of specified field for specified group (like city, state, country etc.) or for the entire table if group is not specified.
The following is an example of a regular SUM() aggregate function. It sums the order amount for each city.
You can see from the result set that a regular aggregate function groups multiple rows into a single output row, which causes individual rows to lose their identity.

�
SELECT city, SUM(order_amount) total_order_amount
FROM [dbo].[Orders] GROUP BY city
�

This does not happen with window aggregate functions. Rows retain their identity and also show an aggregated value for each row. 
In the example below the query does the same thing, namely it aggregates the data for each city and shows the sum of total order amount for each of them. 
However, the query now inserts another column for the total order amount so that each row retains its identity. The column marked grand_total is the new column in the example below.

�
SELECT order_id, order_date, customer_name, city, order_amount,SUM(order_amount) OVER(PARTITION BY city) as grand_total 
FROM [dbo].[Orders]
�

AVG()
AVG or Average works in exactly the same way with a Window function.
The following query will give you average order amount for each city and for each month (although for simplicity we�ve only used data in one month).
We specify more than one average by specifying multiple fields in the partition list.
It is also worth noting that that you can use expressions in the lists like MONTH(order_date) as shown in below query. As ever you can make these expressions as complex as you want so long as the syntax is correct!

�
SELECT order_id, order_date, customer_name, city, order_amount,AVG(order_amount) OVER(PARTITION BY city, MONTH(order_date)) as�� average_order_amount 
FROM [dbo].[Orders]
�

From the above image, we can clearly see that on an average we have received orders of 12,333 for Arlington city for April, 2017.
Average Order Amount = Total Order Amount / Total Orders 
���������������������������������������= (20,000 + 15,000 + 2,000) / 3 
���������������������������������������= 12,333
You can also use the combination of SUM() & COUNT() function to calculate an average.
MIN()
The MIN() aggregate function will find the minimum value for a specified group or for the entire table if group is not specified.
For example, we are looking for the smallest order (minimum order) for each city we would use the following query.

�
SELECT order_id, order_date, customer_name, city, order_amount ,MIN(order_amount) OVER(PARTITION BY city) as minimum_order_amount 
FROM [dbo].[Orders]
�

MAX()
Just as the MIN() functions gives you the minimum value, the MAX() function will identify the largest value of a specified field for a specified group of rows or for the entire table if a group is not specified.
let�s find the biggest order (maximum order amount) for each city.

�
SELECT order_id, order_date, customer_name, city, order_amount,MAX(order_amount) OVER(PARTITION BY city) as maximum_order_amount 
FROM [dbo].[Orders] 
�

COUNT()
The COUNT() function will count the records / rows.

Now, let�s find the total order received for each city using window COUNT() function.

�
SELECT order_id, order_date, customer_name, city, order_amount,COUNT(order_id) OVER(PARTITION BY city) as total_orders
FROM [dbo].[Orders]

Ranking Window Functions
RANKING functions will rank the values of a specified field and categorize them according to their rank.
The most common use of RANKING functions is to find the top (N) records based on a certain value. For example, Top 10 highest paid employees, Top 10 ranked students, Top 50 largest orders etc.
The following are supported RANKING functions:
RANK(), DENSE_RANK(), ROW_NUMBER(), NTILE() 

RANK()
The RANK() function is used to give a unique rank to each record based on a specified value, for example salary, order amount etc.
If two records have the same value then the RANK() function will assign the same rank to both records by skipping the next rank. 
This means � if there are two identical values at rank 2, it will assign the same rank 2 to both records and then skip rank 3 and assign rank 4 to the next record.
Let�s rank each order by their order amount.�
SELECT order_id,order_date,customer_name,city, 
RANK() OVER(ORDER BY order_amount DESC) [Rank]
FROM [dbo].[Orders]
�
DENSE_RANK()
The DENSE_RANK() function is identical to the RANK() function except that it does not skip any rank. 
This means that if two identical records are found then DENSE_RANK() will assign the same rank to both records but not skip then skip the next rank.
Let�s see how this works in practice.
SELECT order_id,order_date,customer_name,city, order_amount,
DENSE_RANK() OVER(ORDER BY order_amount DESC) [Rank]
FROM [dbo].[Orders]
�

ROW_NUMBER()
The name is self-explanatory. These functions assign a unique row number to each record.
The row number will be reset for each partition if PARTITION BY is specified. Let�s see how ROW_NUMBER() works without PARTITION BY and then with PARTITION BY.
ROW_ NUMBER() without PARTITION BY
SELECT order_id,order_date,customer_name,city, order_amount,
ROW_NUMBER() OVER(ORDER BY order_id) [row_number]
FROM [dbo].[Orders]
ROW_NUMBER() with PARTITION BY
SELECT order_id,order_date,customer_name,city, order_amount,
ROW_NUMBER() OVER(PARTITION BY city ORDER BY order_amount DESC) [row_number]
FROM [dbo].[Orders]
�

NTILE()
NTILE() is a very helpful window function. It helps you to identify what percentile (or quartile, or any other subdivision) a given row falls into.
This means that if you have 100 rows and you want to create 4 quartiles based on a specified value field you can do so easily and see how many rows fall into each quartile.
In the query below, we have specified that we want to create four quartiles based on order amount. We then want to see how many orders fall into each quartile.
SELECT order_id,order_date,customer_name,city, order_amount,
NTILE(4) OVER(ORDER BY order_amount) [row_number]
FROM [dbo].[Orders]
�

NTILE creates tiles based on following formula:
No of rows in each tile = number of rows in result set / number of tiles specified
Here is our example, we have total 10 rows and 4 tiles are specified in the query so number of rows in each tile will be 2.5 (10/4). 
As number of rows should be whole number, not a decimal. SQL engine will assign 3 rows for first two groups and 2 rows for remaining two groups.�
